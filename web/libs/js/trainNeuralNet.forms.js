var getSessionKey = function(){
    return
}
$(document).ready(function(){
    console.log('Train Neural Net : Attaching Form Handler')
    $.netTrainer.dropZoneElement = $("#dropUploadArea")
    $.netTrainer.setuUploadDropArea = function(){
        $.netTrainer.dropZoneElement.dropzone({
            url: '/' + $user.sessionKey + '/trainer/jpegImages/upload',
            acceptedFiles: "image/jpeg,image/jpg",
            init: function() {
                this.on("addedfile", function(file) {
                    $.netTrainer.dropZoneElement.find('.dz-preview').remove()
                })
            }
        })
    }
    $.netTrainer.classesField = $('[yolo-option="classes"]')
    $.netTrainer.includeOpenImagesField = $('[yolo-option="includeOpenImages"]')
    $.netTrainer.saveClassesToLocalStorage = function(){
        localStorage.setItem('trainingNetImageClassesSelected',JSON.stringify($.netTrainer.getAddedClassTags(false)))
    }
    $.netTrainer.classesField.tagEditor({
        onChange: function(field, editor, tags) {
            $.netTrainer.saveClassesToLocalStorage()
            $.netTrainer.createDarknetCfg()
        },
        initialTags: JSON.parse(localStorage.getItem('trainingNetImageClassesSelected')) || [
            // "Handgun",
            // "Rifle",
            // "Shotgun",
            // "Knife",
            // "Sword",
            // "Weapon",
            // "Screwdriver",
            // "Baseball bat",
            // "Flashlight",
            // "Dagger",
            // "Hammer",
            // "Torch",
            // "Person",
        ]
    })
    $.netTrainer.getAddedClassTags = function(capitalizeClassName){
        var classes = $.netTrainer.classesField.tagEditor('getTags')[0].tags
        classes.forEach(function(value,key){
            if(capitalizeClassName === true){
                classes[key] = value.charAt(0).toUpperCase() + value.slice(1)
            }else{
                classes[key] = value.toLowerCase()
            }
        })
        return classes
    }
    $.netTrainer.minifyAnnotationData = function(){
        var list = []
        $.each($.netTrainer.getLoadedImagesWithBoundingBoxes(),function(n,image){
            list.push({
                annote: image.dataAnnotation,
                url: image.url,
                source: image.source
            })
        })
        return list
    }
    $.netTrainer.getYoloFormValues = function(){
        var form = {}
        $.netTrainer.e.find('[yolo-option]').each(function(){
            var key = $(this).attr('yolo-option')
            var value = $(this).val()
            form[key] = value
        })
        if(!form.gpus){
            form.gpus = '0'
        }else{
            form.gpus = form.gpus.join(',')
        }
        form.classes = $.netTrainer.getAddedClassTags(true)
        return form
    }
    $.netTrainer.startTraining = function(){
        $.confirm.e.modal('show')
        $.confirm.title.text(lang['Trainer Engine'])
        var html = lang.TrainConfirm
        $.confirm.body.html(html)
        $.confirm.click({title:lang['Train'],class:'btn-danger'},function(){
            var form = $.netTrainer.getYoloFormValues()
            $.post('/' + $user.sessionKey+'/trainer/train',{
                data : JSON.stringify(form),
            },function(data){
                console.log(data)
            })
        })
    }
    $.netTrainer.stopTraining = function(){
        $.confirm.e.modal('show')
        $.confirm.title.text(lang['Trainer Engine'])
        var html = lang.TrainConfirmStop
        $.confirm.body.html(html)
        $.confirm.click({title:lang['Stop'],class:'btn-danger'},function(){
            var form = $.netTrainer.getYoloFormValues()
            $.post('/' + $user.sessionKey+'/trainer/train/stop',{
                data : JSON.stringify(form),
            },function(data){
                console.log(data)
            })
        })
    }
    $.netTrainer.startOpenImagesDownload = function(){
        $.confirm.e.modal('show')
        $.confirm.title.text(lang['OpenImages Downloader'])
        var html = userHasSubscribed ? lang.openImagesDownloadConfirm : lang.onlyAvailableToSubscribers + ' ' + lang.onlyAvailableToSubscribers2
        $.confirm.body.html(html)
        if(userHasSubscribed){
            $.confirm.click({title:lang['Download'],class:'btn-danger'},function(){
                var form = $.netTrainer.getYoloFormValues()
                $.post('/' + $user.sessionKey+'/trainer/openImagesDownload/start',{
                    data : JSON.stringify(form),
                },function(data){
                    console.log(data)
                })
            })
        }else{
            $.confirm.e.find('.confirmaction').hide()
        }
    }
    $.netTrainer.stopOpenImagesDownload = function(){
        $.get('/' + $user.sessionKey+'/trainer/openImagesDownload/stop',function(data){
            console.log(data)
        })
    }
    $.netTrainer.buildModels = function(){
        $.get('/' + $user.sessionKey + '/models/create',function(data){
            console.log(data)
        })
    }
    $.netTrainer.deleteModels = function(){
        $.get('/' + $user.sessionKey + '/models/clean',function(data){
            console.log(data)
        })
    }
    $.netTrainer.createDarknetCfg = function(callback){
        var form = $.netTrainer.getYoloFormValues()
        $.post('/' + $user.sessionKey + '/trainer/darknetCfg/create',{
            data : JSON.stringify(form),
        },function(data){
            if(callback)callback(data)
        })
    }
    $.netTrainer.getDarknetCfg = function(callback){
        var form = $.netTrainer.getYoloFormValues()
        $.get('/' + $user.sessionKey + '/trainer/darknetCfg/get?base=' + form.darknetBase,function(data){
            console.log(data)
            if(callback)callback(data)
        })
    }
    $('body').on('click','.startTraining',function(){
        $.netTrainer.startTraining()
    })
    .on('click','.stopTraining',function(){
        $.netTrainer.stopTraining()
    })
    .on('click','.startOpenImagesDownload',function(){
        $.netTrainer.startOpenImagesDownload()
    })
    .on('click','.stopOpenImagesDownload',function(){
        $.netTrainer.stopOpenImagesDownload()
    })
    .on('click','.buildModels',function(){
        $.netTrainer.buildModels()
    })
    .on('click','.deleteModels',function(){
        $.netTrainer.deleteModels()
    })
    .on('change','#presetYoloOptions',function(){
        var selectedPreset = $(this).val()
        if(presetYoloOptions[selectedPreset]){
          $.each(presetYoloOptions[selectedPreset].options,function(n,option){
              $.netTrainer.e.find(`[yolo-option="${option.name}"]`).val(option.value)
              $.netTrainer.localStore(option.name,option.value,'clientAreaYoloOptions')
              $.netTrainer.createDarknetCfg()
          })
        }
    })
    .find('.createDarknetCfg').click(function(){
        $.netTrainer.createDarknetCfg()
    })
    $('body').find('.loadLocalImages').click(function(){
        $.netTrainer.loadLocalImages()
    })
    $.netTrainer.createAnnotationFiles = function(callback){
        var form = $.netTrainer.getYoloFormValues()
        form.images = $.netTrainer.getLoadedImagesWithBoundingBoxes(true)
        $.netTrainer.cx({
            f: 'buildImageAnnotations',
            form: form
        })
        // $.post('/' + $user.sessionKey + '/trainer/imageAnnotations/build',{
        //     data : JSON.stringify(form),
        // },function(data){
        //     if(callback)callback(data)
        // })
    }
    $.netTrainer.e.find('.createAnnotationFiles').click(function(){
        $.netTrainer.createAnnotationFiles()
    })
    $.netTrainer.e.find('[yolo-option]').change(function(){
        $.netTrainer.createDarknetCfg()
        $.netTrainer.localStore($(this).attr('yolo-option'),$(this).val(),'clientAreaYoloOptions')
    })
    $.each($.netTrainer.localStore(null,null,'clientAreaYoloOptions'),function(key,value){
        $.netTrainer.e.find(`[yolo-option="${key}"]`).val(value)
    })
})
