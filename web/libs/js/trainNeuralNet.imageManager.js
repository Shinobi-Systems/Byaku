$(document).ready(function(){
    $.imageManager = {}
    var foundImageList = $.netTrainer.matrixEditorWindow.find('.images_search_found')
    var buildImageSearchPreviewItem = function(pic){
        var filename = pic.url.split('/')
        filename = filename[filename.length - 1]
        var imageId = pic.source + filename
        var additonalClasses = []
        if(pic.hasAnnotation)additonalClasses.push('hasAnnotation')
        var existingElement = foundImageList.find(`[imageId="${imageId}"]`)
        if(existingElement.length > 0){
            existingElement.remove()
        }
        var html = `<div class="imageSearchPreview unLoadScrolled ${additonalClasses.join(' ')}" imageId="${imageId}" url="${pic.url}" source="${pic.source}">
            <div class="corner-control">
                <a class="btn btn-sm btn-danger delete text-white" title="${lang['Delete']}" href="${pic.url}/delete"><i class="fa fa-trash-o"></i></a>
                <a class="btn btn-sm btn-info test text-white" target="_blank" title="${lang['Object Detection Test']}" href="${$user.sessionKey}${pic.url}/test"><i class="fa fa-search"></i></a>
            </div>
            <small>${pic.source}</small>
            <img data-src="${pic.preview}" class="lazyload">
        </div>`
        return html
    }
    $.imageManager.buildImageSearchPreviewItem = buildImageSearchPreviewItem
})
