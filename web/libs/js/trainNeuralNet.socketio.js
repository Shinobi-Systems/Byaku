$(document).ready(function(){
    console.log('Byaku : Attaching Socket.IO Handler')
    var statusWindow = $('#statusWindow')
    var openImagesDownloadOutputElement = statusWindow.find('.openImagesDownload_output')
    var createDarknetCfgOutputElement = statusWindow.find('.createDarknetCfg_output')
    $.netTrainer.trainingOutputElement = statusWindow.find('.training_output')
    $.netTrainer.trainingOutputElementGood = statusWindow.find('.good')
    $.netTrainer.trainingOutputElementParsed = statusWindow.find('.parsed_data')
    $.netTrainer.trainingErrorElement = statusWindow.find('.training_error')
    $.netTrainer.openImagesDownloadButton = $.netTrainer.e.find('.openImagesDownloadButton')
    $.netTrainer.drawjpegImageUploaded = true
    $.netTrainer.ws = io({
        transports: ['websocket']
    })
    $.netTrainer.cx = function(x){
        return $.netTrainer.ws.emit('super',x)
    }
    $.netTrainer.ws.on('connect',function(d){
        $.netTrainer.cx({
            f: 'init',
            mail: $user.mail,
            pass: $user.pass
        })
    })
    $.netTrainer.ws.on('notification',function(data){
        new PNotify(data)
    })
    $.netTrainer.ws.on('f',function(d){
        switch(d.f){
            case'init_success':
                //image uploader
                $.netTrainer.setuUploadDropArea()
                if(d.engineStatus.training === true){
                    $.netTrainer.trainingButton
                        .removeClass('startTraining')
                        .addClass('stopTraining')
                        .html($.createMenuItemContents({
                            title: lang['Stop Training'],
                            text: lang.trainStopText,
                            type: 'danger',
                            icon: 'square'
                        }))
                        new PNotify({
                            type: 'info',
                            sticky:true,
                            title: lang.trainingInProgress,
                            text: lang.trainingInProgressText
                        })
                }
                if(d.engineStatus.downloadingOpenImages === true){
                    $.netTrainer.openImagesDownloadButton
                        .removeClass('startOpenImagesDownload')
                        .addClass('stopOpenImagesDownload')
                        .html($.createMenuItemContents({
                            title: lang['Stop Downloading OpenImages'],
                            text: lang.downloadOpenImagesText,
                            type: 'danger',
                            icon: 'square'
                        }))
                }
            break;
            case'openImagesDownload':
                switch(d.ff){
                    case'start':
                        openImagesDownloadOutputElement.empty()
                        $.netTrainer.openImagesDownloadButton
                            .removeClass('startOpenImagesDownload')
                            .addClass('stopOpenImagesDownload')
                            .html($.createMenuItemContents({
                                title: lang['Stop Downloading OpenImages'],
                                text: lang.downloadOpenImagesText,
                                type: 'danger',
                                icon: 'square'
                            }))
                    break;
                    case'stop':
                        openImagesDownloadOutputElement.append('<div class="text-center">-------------------------</div>')
                        $.netTrainer.openImagesDownloadButton
                            .addClass('startOpenImagesDownload')
                            .removeClass('stopOpenImagesDownload')
                            .html($.createMenuItemContents({
                                title: lang['Download OpenImages'],
                                text: lang.downloadOpenImagesText,
                                type: 'info',
                                icon: 'download'
                            }))
                    break;
                    case'stdout':
                        if(d.data)openImagesDownloadOutputElement.append('<div>' + d.data + '</div>')
                        openImagesDownloadOutputElement.scrollTop(openImagesDownloadOutputElement[0].scrollHeight);
                    break;
                }
            break;
            case'createDarknetCfg':
                createDarknetCfgOutputElement.html(d.response.cfg.replace(/\n/g,'<br>'))
            break;
            case'jpegImageUploaded':
                if($.netTrainer.drawjpegImageUploaded){
                    $.netTrainer.appendImageSearchPreviewItem($.imageManager.buildImageSearchPreviewItem(d))
                    $.netTrainer.loadedImages[d.source + d.name] = {
                        url: d.url,
                        source: d.source,
                        preview: d.preview,
                        boundingBoxes: d.boundingBoxes || null,
                        className: d.className || null,
                        dimensions: d.dimensions,
                        annotation: d.annotation || null
                    }
                    if(d.boundingBoxes)$.netTrainer.saveLoadedImageDataToLocalStorage()
                }
            break;
            case'jpegImageDeleted':
                var imageId = d.source + d.name
                $.netTrainer.e.find('.imageSearchPreview[imageId="' + imageId +'"]').remove()
                delete($.netTrainer.loadedImages[imageId])
                $.netTrainer.saveLoadedImageDataToLocalStorage()
            break;
            case'trainingProcess':
                switch(d.ff){
                    case'start':
                        $.netTrainer.trainingOutputElementGood.empty()
                        $.netTrainer.trainingOutputElementParsed.empty()
                        $.netTrainer.trainingErrorElement.empty()
                        $.netTrainer.trainingButton
                            .removeClass('startTraining')
                            .addClass('stopTraining')
                            .html($.createMenuItemContents({
                                title: lang['Stop Training'],
                                text: lang.trainStopText,
                                type: 'danger',
                                icon: 'square'
                            }))
                    break;
                    case'starting':
                        $.netTrainer.trainingErrorElement.empty().append('<div class="text-center">' + lang.trainingStarting + '</div>')
                    break;
                    case'stop':
                        $.netTrainer.trainingOutputElementGood.append('<div class="text-center">' + lang.trainingStopped + '</div>')
                        $.netTrainer.trainingOutputElementGood.scrollTop($.netTrainer.trainingOutputElementGood[0].scrollHeight)
                        $.netTrainer.trainingButton
                            .addClass('startTraining')
                            .removeClass('stopTraining')
                            .html($.createMenuItemContents({
                                title: lang['Start Training'],
                                text: lang.trainText,
                                type: 'success',
                                icon: 'play'
                            }))
                    break;
                    // case'stdoutParsed':
                    //     $.netTrainer.trainingOutputElementParsed.html('<div class="list-item">' + rawData + '</div>')
                    // break;
                    case'stdout':
                        var data = d.data.replace(/\)/g,'')
                        // var info = {
                        //     Good: '',
                        //     Bad: '',
                        // }
                        if(data.length > 5){
                            // var infoParts = data.match(/(?<=IOU\:\s*).*?(?=\s*, )/gs)
                            // infoParts.forEach(function(avgValue){
                            //     if(avgValue.indexOf('-nan') === -1){
                            //         icon = 'check'
                            //         iconColor = 'green'
                            //         info.Good += `<i class="fa fa-${icon}" style="color:${iconColor}"></i> &nbsp; Avg IOU: ` + avgValue + `<br><span class="time">${new Date()}</span><br>`
                            //     }
                            // })
                            var rawData = ''
                            data.split(',').forEach(function(value){
                                if(value.indexOf('avg') > -1 || value.indexOf('rate') > -1 || value.indexOf('images') > -1 || value.indexOf('seconds') > -1)rawData += value + '<br>'
                            })
                        }
                        // var outputWindow = $.netTrainer.trainingOutputElementGood
                        // outputWindow.append('<div class="list-item">' + info.Good + '</div>')
                        // outputWindow.scrollTop(outputWindow[0].scrollHeight)
                        // $.netTrainer.trainingOutputElementGood.html('<div class="list-item">' + info.Good + '</div>')
                        if(rawData)$.netTrainer.trainingOutputElementParsed.html('<div class="list-item">' + rawData + '</div>')
                    break;
                    case'stderr':
                        if(!$.statusWindow.hidden){
                            var data = d.data.replace(/\)/g,'')
                            var rawData = ''
                            if(data.length > 5){
                                var rawData = ''
                                data.split(',').forEach(function(value){
                                    if(value.indexOf('total_loss') > -1){
                                        $.statusWindow.lossChartAddNewValue(parseFloat(value.split(' = ')[1]))
                                    }
                                    if(value.indexOf('Avg') > -1 || value.indexOf('rate') > -1 || value.indexOf('images') > -1 || value.indexOf('seconds') > -1)rawData += value + '<br>'
                                })
                            }
                            if(rawData)$.netTrainer.trainingOutputElementParsed.html('<div class="list-item">' + rawData + '</div>')
                        }
                        if(d.data.indexOf('out of memory') > -1){
                            new PNotify({
                                type: 'danger',
                                hide: false,
                                title: lang.trainingOutOfMemory,
                                text: lang.trainingOutOfMemoryText
                            })
                        }
                        $.netTrainer.trainingErrorElement.append('<div class="log-item">' + d.data + '</div>')
                        if($.netTrainer.trainingErrorElement.find('.log-item').length>10){$.netTrainer.trainingErrorElement.find('.log-item:first').remove()}
                        $.netTrainer.trainingErrorElement.scrollTop($.netTrainer.trainingErrorElement[0].scrollHeight)
                    break;
                }
            break;
            case'training_stdout':
            case'training_stderr':
            case'searchPixaBay':
            case'stopTrainingProcess':
            case'openImagesDownload':
            case'downloadMarkedInternetImages':
                console.log(d)
            break;
        }
    })
})
