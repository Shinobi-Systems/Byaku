$(document).ready(function(){
    $.configEditor = {
        e: $('#configurationEditor')
    }
    $.configEditor.e.submit(function(e){
        e.preventDefault();
        $.post(`/${$user.sessionKey}/system/config`,$(this).serializeObject(),function(data){
            if(data && data.ok){
                new PNotify({
                    type: 'success',
                    title: lang.ConfigurationSaved,
                    text: lang.restartByakuForChanges
                })
                $.configEditor.e.modal('hide')
            }
        })
        return false;
    })
})
