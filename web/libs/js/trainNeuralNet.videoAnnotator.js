$(document).ready(function(){
    $.videoAnnotator = {}
    $.videoAnnotator.e = $("#videoAnnotator")
    $.videoAnnotator.matrixEditor = $.videoAnnotator.e.find(".matrix-editor")
    $.videoAnnotator.uploadElement = $("#videoAnnotatorDropUploadArea")
    $.videoAnnotator.remoteUrlInput = $("#videoAnnotatorRemoteUrlInput")
    $.videoAnnotator.shinobiUrlInput = $("#videoAnnotatorShinobiUrlInput")
    frameAnnotationsInMemory = JSON.parse(localStorage.getItem('frameAnnotationsInMemory')) || {}
    frameBuffersInMemory = {}
    var loadedVideo
    var videoPlaying = false
    var seekBar = $.videoAnnotator.e.find(".seekbar")
    var seekBarSpan = seekBar.find("span")
    var getAllSetFrameTimes = function(){
        var matrixContainers = frameAnnotationsInMemory[loadedVideo.id]
        return Object.keys(matrixContainers)
    }
    var scanFrames = function(){
        var lastTime = -1
        var currentVideo = loadedVideo.element[0]
        var ratios = getVideoRatios()
        function getFrame() {
            var time = currentVideo.currentTime
            if (time !== lastTime) {
                var currentTimeInt = roundToHalf(time)
                var matrices = frameAnnotationsInMemory[loadedVideo.id][currentTimeInt]
                if(matrices){
                    var matrixSpeed = 10
                    var setFrameKeys = getAllSetFrameTimes(loadedVideo)
                    var arrayPositionOfFrame = setFrameKeys.indexOf(currentTimeInt)
                    if(arrayPositionOfFrame > 0){
                        matrixSpeed = (currentTimeInt - roundToHalf(setFrameKeys[arrayPositionOfFrame - 1])) * 1000 || 10
                    }
                    $.videoAnnotator.e.find('[matrix-id]').each(function(n,el){
                        var matrixElement = $(el)
                        var matrixId = matrixElement.attr('matrix-id')
                        var foundMatrix = matrices.filter(function(matrix){return matrix.id === matrixId}).length
                        if(!foundMatrix)matrixElement.addClass('matrix-hidden')
                    })
                    matrices.forEach(function(matrix){
                        moveMatrix(matrix,ratios,matrixSpeed)
                    })
                }
                lastTime = time
            }
            if(videoPlaying)setTimeout(getFrame,1000 / 25)
        }
        getFrame()
    }
    var renderUrlOnToCanvas = function (theUrl) {
        $.videoAnnotator.matrixEditor.html(`<div class="matrices"></div><video crossorigin="anonymous" class="annotation-video" preload><source crossorigin="anonymous" src="${theUrl}" type="video/mp4"></source></video>`)
        var smallSlice = theUrl.slice(0,50)
        loadedVideo = {
            id: smallSlice.indexOf('data:video') > -1 ? smallSlice : theUrl,
            href: theUrl,
            element: $.videoAnnotator.matrixEditor.find('.annotation-video')
        }
        if(!frameBuffersInMemory[loadedVideo.id])frameBuffersInMemory[loadedVideo.id] = {}
        if(!frameAnnotationsInMemory[loadedVideo.id])frameAnnotationsInMemory[loadedVideo.id] = {}
        var vid = loadedVideo.element[0]
        loadedVideo.element.on('timeupdate',function(){
            var percentage = roundToHalf(( vid.currentTime / vid.duration ) * 100)
            seekBarSpan.css("width", percentage + "%")
        })
    }
    var renderVideoOnToCanvas = function (file) {
        var reader = new FileReader()
        reader.onload = function(event) {
            theUrl = event.target.result
            renderUrlOnToCanvas(theUrl)
        }
        reader.readAsDataURL(file)
    }
    var getVideoRatios = function(){
        var imageEl = $.videoAnnotator.matrixEditor.find('.annotation-video')
        var imageWidthInView = imageEl.width()
        var imageHeightInView = imageEl.height()
        imageEl.css({'max-width':'none!important'})
        var imageWidth = imageEl[0].videoWidth
        var imageHeight = imageEl[0].videoHeight
        imageEl.css({'max-width':'100%!important'})
        var xRatio = imageWidth / imageWidthInView
        var yRatio = imageHeight / imageHeightInView
        return {
            xRatio: xRatio,
            yRatio: yRatio
        }
    }
    var getCurrentPositionOfMatrices = function(){
        var overallClassesLoaded = $.netTrainer.getAddedClassTags(false)
        var matriceData = {}
        var matricesFoundInView = {
            boundingBoxes: []
        }
        var matricesContainer = $.videoAnnotator.matrixEditor.find('.matrices:visible')
        var matricesInView = matricesContainer.find('.matrix')
        var imageEl = $.videoAnnotator.matrixEditor.find('.annotation-video')
        var imageWidthInView = imageEl.width()
        var imageHeightInView = imageEl.height()
        imageEl.css({'max-width':'none!important'})
        var imageWidth = imageEl[0].videoWidth
        var imageHeight = imageEl[0].videoHeight
        imageEl.css({'max-width':'100%!important'})
        var xRatio = imageWidth / imageWidthInView
        var yRatio = imageHeight / imageHeightInView
        var dataAnnotation = []
        matricesInView.each(function(n,el){
            var matrix = $(el)
            var position = matrix.position()
            var width = matrix.width()
            var height = matrix.height()
            var id = matrix.attr('matrix-id')
            var tag = matrix.find('input').val().toLowerCase()
            if(tag !== ''){
                var classAnnotationPosition = overallClassesLoaded.indexOf(tag)
                if(classAnnotationPosition === -1){
                    overallClassesLoaded.push(tag)
                    classAnnotationPosition = overallClassesLoaded.indexOf(tag)
                    $.netTrainer.classesField.tagEditor('addTag',tag, true)
                    $.netTrainer.saveClassesToLocalStorage()
                    $.netTrainer.createDarknetCfg()
                }
                // var matrixAsAnnotation = `${classAnnotationPosition} ${(position.left + (width / 2)) / imageWidthInView} ${(position.top + (height / 2)) / imageHeightInView} ${width / imageWidthInView} ${height / imageHeightInView}`
                var matrixAsAnnotation = [
                    classAnnotationPosition,
                    ((position.left + (width / 2)) / imageWidthInView) * xRatio,
                    ((position.top + (height / 2)) / imageHeightInView) * yRatio,
                    (width / imageWidthInView) * xRatio,
                    (height / imageHeightInView) * yRatio
                ].join(' ')
                dataAnnotation.push(matrixAsAnnotation)
                matricesFoundInView.boundingBoxes.push({
                    id: id,
                    h: height * yRatio,
                    w: width * xRatio,
                    x: position.left * xRatio,
                    y: position.top * yRatio,
                    tag: tag,
                    annote: matrixAsAnnotation
                })
            }else{
                console.log('Object Tag Empty!')
            }
        })
        matriceData.dataAnnotation = dataAnnotation.join('\n')
        matriceData.boundingBoxes = matricesFoundInView.boundingBoxes
        matriceData.imageWidth = imageWidth
        matriceData.imageHeight = imageHeight
        return matriceData
    }
    var downloadFile = function(blob, filename) {
      // if (window.navigator.msSaveOrOpenBlob) {
      //   window.navigator.msSaveOrOpenBlob(blob, filename);
      // } else {
        const a = document.createElement('a');
        document.body.appendChild(a);
        const url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = filename;
        a.click();
        setTimeout(() => {
          window.URL.revokeObjectURL(url);
          document.body.removeChild(a);
        }, 0)
      // }
    }
    var saveImage = function(blob, filename, options, callback) {
        if(!options)options = {}
        var endpoint = '/' + $user.sessionKey + '/trainer/jpegImages/upload'
        var formData = new FormData()
        formData.append("labelFilename", filename + '.txt')
        if(options.matrices && options.matrices.length > 0){
            if(options.matrices)formData.append("matrices", JSON.stringify(options.matrices))
            if(options.height)formData.append("height", options.height)
            if(options.width)formData.append("width", options.width)
            formData.set("file", blob , filename + '.jpg')
            $.ajax({
                url: endpoint,
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function(response){
                    if(callback)callback(response)
                }
            })
        }
    }
    var snapshotVideo = function(videoElement){
        var image_data
        var base64
        var c = document.createElement("canvas")
        var img = videoElement
        var width = img.videoWidth
        var height = img.videoHeight
        c.width = width
        c.height = height
        var ctx = c.getContext('2d')
        ctx.drawImage(img, 0, 0,c.width,c.height)
        var base64 = c.toDataURL('image/jpeg')
        var image_data = atob(base64.split(',')[1])
        var arraybuffer = new ArrayBuffer(image_data.length)
        var view = new Uint8Array(arraybuffer)
        for (var i=0; i<image_data.length; i++) {
            view[i] = image_data.charCodeAt(i) & 0xff
        }
        try {
            var blob = new Blob([arraybuffer], {type: 'application/octet-stream'})
        } catch (e) {
            var bb = new (window.WebKitBlobBuilder || window.MozBlobBuilder)
            bb.append(arraybuffer)
            var blob = bb.getBlob('application/octet-stream')
        }
        return {
            blob: blob,
            buffer: image_data,
            width: width,
            height: height
        }
    }
    var generateFramesAndLabelsFromVideoOnCanvasAndMemory = function(){
        var videoElement = loadedVideo.element[0]
        var width = videoElement.videoWidth
        var height = videoElement.videoHeight
        var matrixContainers = frameAnnotationsInMemory[loadedVideo.id]
        var imageContainers = frameBuffersInMemory[loadedVideo.id]
        var generatedData = []
        if(!videoElement.paused)videoElement.pause()
        var markedTimes = getAllSetFrameTimes()
        var step = 0
        // for (let step = 0; step < markedTimes.length; step++)
        var checkFrames = function(){
            var time = markedTimes[step]
            if(time){
                var matrices = matrixContainers[time]
                if(matrices){
                    var image = imageContainers[time]
                    var completeAction = function(){
                      var tagNames = []
                      matrices.forEach(function(matrix){
                          if(tagNames.indexOf(matrix.tag) > -1)tagNames.push(matrix.tag)
                      })
                      var filename = (new Date()).getTime() + '_' + tagNames.join('-') + '_' + time
                      generatedData.push({
                          image: image,
                          matrices: Object.assign({},matrices)
                      })
                      saveImage(image.blob,filename,{
                          matrices: matrices,
                          width: width,
                          height: height,
                      },function(response){
                          ++step
                          checkFrames()
                      })
                    }
                    if(!image){
                        setVideoTime(time)
                        setTimeout(function(){
                          imageContainers[time] = snapshotVideo(videoElement)
                          image = imageContainers[time]
                          completeAction()
                        },1000)
                    }else{
                        completeAction()
                    }
                }
            }else{
                $.netTrainer.createAnnotationFiles()
            }
        }
        checkFrames()
        return generatedData
    }
    // DEV ONLY >
    window.saveImage = saveImage
    window.snapshotVideo = snapshotVideo
    window.generateFramesAndLabelsFromVideoOnCanvasAndMemory = generateFramesAndLabelsFromVideoOnCanvasAndMemory
    window.getCurrentPositionOfMatrices = getCurrentPositionOfMatrices
    window.frameBuffersInMemory = frameBuffersInMemory
    window.frameAnnotationsInMemory = frameAnnotationsInMemory
    // DEV ONLY />
    function roundToHalf(x){
        return x
    }
    var saveMatricesOnTime = function(){
        var video = $.videoAnnotator.matrixEditor.find('.annotation-video')[0]
        var currentTimeInt = roundToHalf(video.currentTime)
        var boundingBoxes = getCurrentPositionOfMatrices().boundingBoxes
        if(frameBuffersInMemory[loadedVideo.id][currentTimeInt])frameBuffersInMemory[loadedVideo.id][currentTimeInt] = snapshotVideo(video)
        frameAnnotationsInMemory[loadedVideo.id][currentTimeInt] = boundingBoxes
        localStorage.setItem('frameBuffersInMemory',JSON.stringify(frameBuffersInMemory))
    }
    var moveMatrix = function(matrix,ratios,matrixSpeed){
        if(!matrix)matrix = {}
        if(!matrix.tag)matrix.tag = ''
        var theMatrix = $.videoAnnotator.matrixEditor.find(`[matrix-id="${matrix.id}"]`)
        if(theMatrix.length === 0){
            theMatrix = addNewMatrixToEditor(matrix)
        }else{
            theMatrix.css({
                width: matrix.w / ratios.xRatio,
                height: matrix.h / ratios.yRatio,
                top: matrix.y / ratios.yRatio,
                left: matrix.x / ratios.xRatio
            }).removeClass('matrix-hidden')
        }
    }
    var buildMatrix = function(matrix){
        if(!matrix)matrix = {}
        if(!matrix.id)matrix.id = $.randomString(5)
        if(!matrix.w)matrix.w = '100'
        if(!matrix.h)matrix.h = '100'
        // if(!matrix.x)matrix.x = '100'
        // if(!matrix.y)matrix.y = '100'
        if(!matrix.tag)matrix.tag = ''
        var html = `<div matrix-id="${matrix.id}" class='matrix selected' style="left: ${matrix.x}px; top: ${matrix.y}px; width: ${matrix.w}px; height: ${matrix.h}px;">
                      <div class="handle">
                          <a class="removeMatrixFromFrame pull-right float-right">&nbsp;<i class="fa fa-times"></i>&nbsp;</a>
                          <!-- <a class="removeMatrixFromAllFrames pull-right float-right">&nbsp;<i class="fa fa-times-circle-o"></i>&nbsp;</a> -->
                      </div>
                      <input class="form-control object-tag" value="${matrix.tag}" placeholder="Object Tag">
                      <div class='resizers'>
                        <div class='resizer bottom-right'></div>
                      </div>
                    </div>`
        return html
    }
    var activateMatrix = function(newMatrixElement,matrix,xRatio,yRatio){
        newMatrixElement.draggable({
            handle: ".handle",
            containment: "parent",
            start: function() {
                saveMatricesOnTime()
            },
            stop: function() {
                saveMatricesOnTime()
            }
        })
        newMatrixElement.find('.object-tag').change(function(){
            saveMatricesOnTime()
        })
        if(matrix){
            if(!matrix.w)matrix.w = 100
            if(!matrix.h)matrix.h = 100
            if(!matrix.x)matrix.x = 100
            if(!matrix.y)matrix.y = 100
            if(!matrix.tag)matrix.tag = ''
            newMatrixElement.css({
                width: matrix.w / xRatio + 'px',
                height: matrix.h / yRatio + 'px',
                top: matrix.y / yRatio + 'px',
                left: matrix.x / xRatio + 'px',
            })
        }
        newMatrixElement.resizable({
            containment: "#videoAnnotator .matrix-editor .matrices",
            handles: 'n, e, s, w, ne, nw, se, sw',
            stop: function() {
                saveMatricesOnTime()
            }
        })

    }
    var addNewMatrixToEditor = function(options){
        var matrixOptions = Object.assign({
            tag: ''
        },options)
        var matricesContainer = $.videoAnnotator.matrixEditor.find('.matrices')
        matricesContainer.find('.matrix').removeClass('selected')
        matricesContainer.append(buildMatrix(matrixOptions))
        var newMatrixElement = matricesContainer.find('.matrix').last()
        activateMatrix(newMatrixElement)
        return newMatrixElement
    }
    var removeMatrixFromEditor = function(options){
        var matrixOptions = Object.assign({
            tag: ''
        },options)
        var matricesContainer = $.videoAnnotator.matrixEditor.find('.matrices')
        matricesContainer.find(`[matrix-id="${matrixOptions.id}"]`).remove()
    }
    var removeMatrixFromAllFrames = function(options){
        var matrixOptions = Object.assign({
            tag: ''
        },options)
        var video = $.videoAnnotator.matrixEditor.find('.annotation-video')[0]
        // var currentTimeInt = roundToHalf(video.currentTime)
        $.each(frameBuffersInMemory[loadedVideo.id],function(currentTimeInt,matrices){
            var newMatrices = []
            matrices.forEach(function(matrix){
                if(matrix.id !== matrixOptions.id){
                    newMatrices.push(matrix)
                }
            })
            frameAnnotationsInMemory[loadedVideo.id][currentTimeInt] = newMatrices
        })
    }
    var playVideoCurrentTime = 0
    var videoPlaying = false
    var playVideoInterval = null
    var playVideo = function(){
        videoPlaying = true
        var video = $.videoAnnotator.matrixEditor.find('.annotation-video')[0]
        playVideoInterval = setInterval(function(){
            playVideoCurrentTime += 0.1
            video.currentTime = playVideoCurrentTime
        },100)
    }
    var pauseVideo = function(){
        videoPlaying = false
        delete(playVideoInterval)
        clearInterval(playVideoInterval)
    }
    var setVideoTime = function(time){
        var video = $.videoAnnotator.matrixEditor.find('.annotation-video')[0]
        playVideoCurrentTime = time
        video.currentTime = time
    }
    $.videoAnnotator.e
        .on('click','.saveMatrices',function(){
            generateFramesAndLabelsFromVideoOnCanvasAndMemory()
        })
        .on('click','.addMatrix',function(){
            addNewMatrixToEditor()
            saveMatricesOnTime()
        })
        .on('click','.removeMatrixFromFrame',function(){
            removeMatrixFromEditor({
              id: $(this).parents('[matrix-id]').attr('matrix-id')
            })
        })
        .on('click','.removeMatrixFromAllFrames',function(){
          removeMatrixFromAllFrames({
            id: $(this).parents('[matrix-id]').attr('matrix-id')
          })
        })
        .on('click','.resetVideoTime',function(){
            pauseVideo()
            setVideoTime(0)
            $.videoAnnotator.e.find('.playPause').html(`<div class="icon icon-sm icon-shape bg-gradient-danger rounded-circle text-white" title="${lang.Play}">
                <i class="fa fa-play"></i>
             </div>`)
        })
        .on('click','.playPause',function(){
            var el = $(this)
            var currentVideo = $.videoAnnotator.e.find('.matrix-editor .annotation-video')[0]
            if(!videoPlaying){
                el.html(`<div class="icon icon-sm icon-shape bg-gradient-info rounded-circle text-white" title="${lang.Pause}">
                            <i class="fa fa-pause"></i>
                         </div>`)
                videoPlaying = true
                scanFrames()
                playVideo()
            }else{
                el.html(`<div class="icon icon-sm icon-shape bg-gradient-danger rounded-circle text-white" title="${lang.Play}">
                            <i class="fa fa-play"></i>
                         </div>`)
                videoPlaying = false
                pauseVideo()
            }
        })
        .on('click','.loadVideoByUrl',function(e){
            e.preventDefault()
            var href = $(this).attr('href')
            renderUrlOnToCanvas(href)
            setVideoTime(0)
            return false
        })
        .on('mouseover','.loaded-api-video-row video',function(e){
            var video = $(this)[0]
            if(video.paused)video.play()
        })
        .on('mouseout','.loaded-api-video-row video',function(e){
            var video = $(this)[0]
            if(!video.paused)video.pause()
        })
    $.videoAnnotator.uploadElement.change(function() {
        renderVideoOnToCanvas(this.files[0])
    })
    $.videoAnnotator.remoteUrlInput.change(function() {
        renderUrlOnToCanvas($(this).val())
    })
    $.videoAnnotator.shinobiUrlInput.change(function() {
        var apiUrl = $(this).val()
        var el = $('#videoAnnotatorShinobiVideos tbody')
        var html = ''
        var urlInfo = document.createElement('a')
        urlInfo.href = apiUrl
        $.ajax({
            type: "GET",
            url: apiUrl,
            error: function(xhr, statusText) {
                html += `<tr><td class="text-center">${lang['No Videos Found']}</td></tr>`
                el.html(html)
            },
            success: function(data){
                if(data && data.videos){
                    $.each(data.videos,function(n,video){
                        var videoHref = urlInfo.origin + video.href
                        html += `<tr class="loaded-api-video-row">
                                    <td><div><small>${video.time}</small></div><div><small>${video.end}</small></div></td>
                                    <td class="text-right"><a href="${videoHref}" class="loadVideoByUrl btn btn-sm btn-primary">${lang['Load Video']}</a></td>
                                 </tr>`
                    })
                }else{
                    html += `<tr><td class="text-center">${lang['No Videos Found']}</td></tr>`
                }
                el.html(html)
            }
        })
    })
    seekBar.on("mouseup", function(e){
        var vid = loadedVideo.element[0]
        var el = $(this)
        var offset = el.offset()
        var left = (e.pageX - offset.left)
        var totalWidth = el.width()
        var percentage = ( left / totalWidth )
        var vidTime = parseFloat((vid.duration * percentage).toFixed(1))
        setVideoTime(vidTime)
    })
})
