var os = require('os')
module.exports = function(process,__dirname){
    process.send = process.send || function () {};
    process.on('uncaughtException', function (err) {
        console.error('Uncaught Exception occured!');
        console.error(err.stack);
    });
    // [CTRL] + [C] = exit
    process.on('SIGINT', function() {
        console.log('Shinobi-Vehicle-Logger : Exiting...')
        process.exit();
    });
    // s = Shinobi
    s = {
        //Total Memory
        coreCount : os.cpus().length,
        //Total Memory
        totalmem : os.totalmem(),
        //Check Platform
        platform : os.platform(),
        //directory path for this file
        mainDirectory : process.cwd()
    }
    return s
}
