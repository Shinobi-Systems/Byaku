var fs = require('fs');
module.exports = function(s,app,lang,config,io){
    s.renderPageHomePassables = {}
    app.get('/', function (req,res){
        s.renderPage(req,res,'pages/index')
    })
    /**
    * API : Login Page POST Handler
    */
    app.post('/', function (req,res){
        var logData = {
            type:'loginFailed',
            details: {
                ip: s.getClientIp(req)
            }
        }
        var username = s.getPostData(req,'name')
        var password = s.getPostData(req,'pass')
        var isEmail = false
        if(username.indexOf('@') > -1){
            isEmail = true
        }
        var sessionKey = s.gid(25)
        s.authenticateWithUsernamePassword({
            name: username,
            pass: password,
            apiKey: sessionKey,
            forceQuery: true,
            ignoreActiveSessions: true
        },function(response){
            var user = response.user
            if(user){
                user.sessionKey = sessionKey
                var renderPageHomePassables = {
                    user: user,
                    gpus: s.gpuDataOnStart
                }
                s.renderPage(req,res,'pages/home',Object.assign(renderPageHomePassables,s.renderPageHomePassables))
            }else{
                //failed, return to login screen
                s.renderPage(req,res,'pages/index',{
                    window: {
                        loginFailed: true
                    }
                })
            }
        })
    })
    app.get('/:apiKey/systemLogs', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            res.end(s.prettyPrint(s.allLogDataInMemory))
        },res,req)
    })
    app.post('/:apiKey/system/config', function (req,res){
        s.authenticateWithUsernamePassword(req.params,function(resp){
            res.setHeader('Content-Type', 'application/json')
            var newConfig = req.body
            delete(newConfig.userHasSubscribed)
            fs.writeFile('./conf.json',s.prettyPrint(newConfig),()=>{})
            res.end(s.prettyPrint({ok: true,newConfig: newConfig}))
        },res,req)
    })
}
