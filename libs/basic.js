var crypto = require('crypto');
var moment = require('moment');
module.exports = function(s,config,lang){
    s.parseJSON = function(string){
        var parsed
        try{
            parsed = JSON.parse(string)
        }catch(err){

        }
        if(!parsed)parsed = string
        return parsed
    }
    s.stringJSON = function(json){
        try{
            if(json instanceof Object){
                json = JSON.stringify(json)
            }
        }catch(err){

        }
        return json
    }
    //JSON stringify short-hand
    s.s = JSON.stringify
    //Pretty Print JSON
    s.prettyPrint = function(obj){
        return JSON.stringify(obj,null,3)
    }
    s.gid = function(x){
        if(!x){x=10};var t = "";var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < x; i++ )
            t += p.charAt(Math.floor(Math.random() * p.length));
        return t;
    }
    s.md5 = function(x){return crypto.createHash('md5').update(x).digest("hex")}
    s.createHash = s.md5
    s.timeObject = moment
    s.debugLog = function(q,w,e){
        if(config.debugLog === true){
            if(!w){w = ''}
            if(!e){e = ''}
            s.appLog(s.timeObject().format(),q,w,e)
            if(config.debugLogVerbose === true){
                s.appLog(new Error())
            }
        }
    }
    s.checkCorrectPathEnding = function(x){
        var length=x.length
        if(x.charAt(length-1)!=='/'){
            x=x+'/'
        }
        return x.replace('__DIR__',s.mainDirectory)
    }
    s.allLogDataInMemory = []
    s.appLog = (arg1,arg2,arg3) => {
        var newDate = new Date()
        var newArray = []
        newArray.push(newDate)
        if(arg1)newArray.push(arg1)
        if(arg2)newArray.push(arg2)
        if(arg3)newArray.push(arg3)
        if(newArray.length > 0)s.allLogDataInMemory.push(newArray)
        if(arg1)console.log(arg1 ? arg1 : '',arg2 ? arg2 : '',arg3 ? arg3 : '')
    }
}
