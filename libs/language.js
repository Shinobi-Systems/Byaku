module.exports = function(s,config){
    if(!config.language){
        config.language='en_CA'
    }
    var languageFolder = __dirname + '/../languages/'
    try{
        var lang = require(languageFolder + config.language+'.js')()
    }catch(er){
        console.log(er)
        console.log('There was an error loading your language file.')
        var lang = require(languageFolder + 'en_CA.js')()
    }

    //load languages dynamically
    s.loadedLanguages={}
    s.loadedLanguages[config.language]=lang;
    s.getLanguageFile = function(rule){
        if(rule && rule !== ''){
            var file = s.loadedLanguages[file]
            if(!file){
                try{
                    s.loadedLanguages[rule] = require(languageFolder + rule+'.js')()
                    s.loadedLanguages[rule] = Object.assign(lang,s.loadedLanguages[rule])
                    file = s.loadedLanguages[rule]
                }catch(err){
                    file = lang
                }
            }
        }else{
            file = lang
        }
        return file
    }
    return lang
}
