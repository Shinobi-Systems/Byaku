var io = new (require('socket.io'))()
//process
var s = require('./libs/process.js')(process,__dirname)
//configuration loader
var config = require('./libs/config.js')(s)
//languages
var lang = require('./libs/language.js')(s,config,lang)
//basic functions
require('./libs/basic.js')(s,config,lang)
//authentication
require('./libs/auth.js')(s,config,lang,io)
//socket.io
require('./libs/webSocket.js')(s,config,lang,io)
//express web server with ejs
var app = require('./libs/webServer.js')(s,config,lang,io)
//shinobi connector
require('./libs/trainer.js')(s,config,lang,app,io)
//shinobi connector
require('./libs/startup.js')(s,config,lang,app,io)

module.exports = {
  s: s,
  config: config,
  lang: lang,
  app: app,
  io: io,
}
